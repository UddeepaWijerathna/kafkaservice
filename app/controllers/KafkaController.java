package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.api.libs.json.Json;
import play.mvc.Controller;
import play.mvc.Http;
import services.KafkaService;

import javax.xml.transform.Result;

public class KafkaController extends Controller {

    public KafkaService kafkaService;

    public Result postMessage() {
        kafkaService.createProducerService();
        return (Result) ok();
    }

    public Result getMessage() {
        kafkaService.createConsumerService();
        return (Result) ok("done");
    }
}
